const requestModal = document.querySelector('.new-request');
const requestLink = document.querySelector('.add-request');
const requestForm = document.querySelector('.new-request form');

// open request modal
requestLink.addEventListener('click', () => {
  requestModal.classList.add('open');
});

// close request modal
requestModal.addEventListener('click', (e) => {
  if (e.target.classList.contains('new-request')) {
    requestModal.classList.remove('open');
  }
});

// add a new request
requestForm.addEventListener('submit', (e) => {
    e.preventDefault();
  
    const addRequest = firebase.functions().httpsCallable('addRequest');
    addRequest({ 
      text: requestForm.request.value 
    })
    .then(() => {
      requestForm.reset();
      requestForm.querySelector('.error').textContent = '';
      requestModal.classList.remove('open');
    })
    .catch(error => {
      requestForm.querySelector('.error').textContent = error.message;
    });
  });


  //Notications
  const notification = document.querySelector('.notification');

  const showNotification = (message) => {
    notification.textContent = message;
    notification.classList.add('active');
    setTimeout(() => {
      notification.classList.remove('active');
      notification.textContent = '';
    }, 4000);
  }



// // Function say Hello callable, Está fué una función de prueba para ejecutar la funció sayHello comentada en el archivo index.js y que estaba sienda llamada en el boton que tiene el name='call', en index.html
// const button = document.querySelector('.call');
// button.addEventListener('click', ()=>{
//     obtine la función de referencia
//     const sayHello = firebase.functions().httpsCallable('sayHello');
//     sayHello({name: 'Raunel'}).then(result =>{
//         console.log(result.data);
//     });
// })

